echo "Domain Blocking"
echo "Enter User Token"
read token
echo "Getting Device entry list"
result=$(curl -s -X GET -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/devices)
if [[ "$result" = *200* ]]
then
echo "Device List: "
echo $result | jq -r ".devices"
else
echo "Cannot get device list"
echo "Error: "
echo $result
exit
fi
echo "Enter Device ID"
read device_id
while read domain; do
echo "Blocking Domain: $domain"
result=$(curl -s -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $token" -d "{\"website\":\"$domain\"}" https://cyberv1p.io.xunison.com/api/v1/parental_control/blocked-domains?device_id=$device_id)
if [[ "$result" == *"FAILURE"* ]]
then
echo "Domain Entry Sending Failed"
echo $result
exit
else
echo "Domain Entry Sent to Backend"
echo $result
fi
echo "waiting for 30 seconds"
sleep 30
echo "Testing if domain is blocked"
dns=$(dig +short $domain)
if [[ "$dns" == "172.33.55.1" ]]
then
echo "**Domain blocked**"
else
echo "**Domain not blocked**"
fi
echo "Getting Domain Entry"
result=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/blocked-domains?device_id=$device_id | jq -r ".blocked_domains[0] .id")
echo "Unblocking Domain: $domain"
result=$(curl -s -X DELETE -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/blocked-domains?entry_id=$result)
if [[ "$result" == *"FAILURE"* ]]
then
echo "Domain Unblocked failed"
echo $result
exit
else
echo "Domain Unblocked"
echo $result
fi
echo "waiting for 30 seconds"
sleep 30
echo "Testing if domain is unblocked"
dns=$(dig +short $domain)
if [[ "$dns" == "172.33.55.1" ]]
then    
echo "**Domain blocked**"
else    
echo "**Domain not blocked**"
fi
done <domains.txt
