echo "Enter User Token"
read token
echo "Getting Device entry list"
result=$(curl -s -X GET -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/devices)
if [[ "$result" = *200* ]]
then
echo "Device List: "
echo $result | jq -r ".devices"
else
echo "Cannot get device list"
echo "Error: "
echo $result
exit
fi
echo "Enter Device ID"
read device_id
echo "Getting Domain Entry"
result=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/blocked-domains?device_id=$device_id | jq ".")
echo "Enter Domain Entry to unblock"
read result
result=$(curl -s -X DELETE -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/blocked-domains?entry_id=$result)
if [[ "$result" == *"FAILURE"* ]]
then
echo "Domain Unblocked failed"
echo $result
exit
else
echo "Domain Unblocked"
echo $result
fi
check_unblock()
{
echo "Testing if domain is unblocked every second"
dns=$(dig +short $domain)
if [[ "$dns" == "172.33.55.1" ]]
then    
echo "**Domain blocked**"
else    
echo "**Domain not blocked**"
fi
}
while true
do
check_unblock
done
