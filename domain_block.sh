echo "Domain Blocking"
echo "Enter User Token"
read token
echo "Getting Device entry list"
result=$(curl -s -X GET -H "Authorization: Bearer $token" https://cyberv1p.io.xunison.com/api/v1/parental_control/devices)
if [[ "$result" = *200* ]]
then
echo "Device List: "
echo $result | jq -r ".devices"
else
echo "Cannot get device list"
echo "Error: "
echo $result
exit
fi
echo "Enter Device ID"
read device_id
echo "Enter Domain to block"
read domain
echo "Blocking Domain: $domain"
result=$(curl -s -X PUT -H "Content-Type: application/json" -H "Authorization: Bearer $token" -d "{\"website\":\"$domain\"}" https://cyberv1p.io.xunison.com/api/v1/parental_control/blocked-domains?device_id=$device_id)
if [[ "$result" == *"FAILURE"* ]]
then
echo "Domain Entry Sending Failed"
echo $result
exit
else
echo "Domain Entry Sent to Backend"
echo $result
fi
check_block()
{
echo "Testing if domain is blocked every second (Press Ctrl+C to exit)"
dns=$(dig +short $domain)
if [[ "$dns" == "172.33.55.1" ]]
then
echo "**Domain blocked**"
else
echo "**Domain not blocked**"
fi
}
while true
do
check_block
done
